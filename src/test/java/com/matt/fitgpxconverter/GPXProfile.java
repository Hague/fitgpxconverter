
package com.matt.fitgpxconverter;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Can be used to overapproximate equality of GPX strings
 *
 * Profiles contents of some key tags, and checks same number of each
 * content string in each file.
 */
class GPXProfile {

    private static final String KEY_TAGS = "time|ele|speed|name|cmt";
    private static final Pattern KEY_TAGS_RE = Pattern.compile(
        "<(" + KEY_TAGS + ")>([^<]*)<\\/\\1>"
    );
    private static final int TAG_CONTENTS_GROUP = 2;

    private Map<String, Integer> profile = new HashMap<>();

    /**
     * Create a profile of a gpx string
     */
    public GPXProfile(String gpx) {
        Matcher m = KEY_TAGS_RE.matcher(gpx);
        while (m.find()) {
            String contents = m.group(TAG_CONTENTS_GROUP);
            int count = profile.getOrDefault(contents, 0);
            profile.put(contents, count + 1);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof GPXProfile))
            return false;
        GPXProfile other = (GPXProfile) o;
        return profile.equals(other.profile);
    }

    @Override
    public int hashCode() {
        return profile.hashCode();
    }
}

