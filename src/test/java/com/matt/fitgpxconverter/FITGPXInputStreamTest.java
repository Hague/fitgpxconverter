
package com.matt.fitgpxconverter;

import java.io.IOException;
import java.lang.StringBuilder;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

public class FITGPXInputStreamTest extends FITGPXStreamTest {

    public FITGPXInputStreamTest(String baseFilename) {
        super(baseFilename);
    }

    /**
     * Read gpx string from .fit file
     */
    @Override
    protected String getFITString(String filename) throws IOException {
        try (
            FITGPXInputStream fis = new FITGPXInputStream(
                getClass().getClassLoader()
                          .getResourceAsStream(filename)
            )
        ) {
            StringBuilder sb = new StringBuilder();
            byte[] bytes = new byte[4096];
            int read;
            do {
                read = fis.read(bytes);
                if (read > -1)
                    sb.append(new String(bytes, 0, read, StandardCharsets.UTF_8));
            } while (read > -1);
            return sb.toString();
        }
    }
}
