
package com.matt.fitgpxconverter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import static org.junit.Assert.*;

public class FITGPXOutputStreamTest extends FITGPXStreamTest {

    public FITGPXOutputStreamTest(String baseFilename) {
        super(baseFilename);
    }

    /**
     * Read gpx string from .fit file
     */
    @Override
    protected String getFITString(String filename) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try (
            InputStream is
                = getClass().getClassLoader()
                            .getResourceAsStream(filename);
            FITGPXOutputStream fos = new FITGPXOutputStream(bos)
        ) {
            byte[] bytes = new byte[4096];
            int read;
            do {
                read = is.read(bytes);
                if (read > -1)
                    fos.write(bytes, 0, read);
            } while (read > -1);
        }
        return bos.toString(StandardCharsets.UTF_8.toString());
    }
}

