
package com.matt.fitgpxconverter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.StringBuilder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import static org.junit.Assert.*;

/**
 * Base class for io stream tests
 */
@RunWith(Parameterized.class)
abstract class FITGPXStreamTest {

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
            { "2014-11-12-15-33-25" },
            { "530-breath" },
            { "67HJ5546" },
            { "682I3734" },
            { "68MK1405" },
        });
    }

    private String baseFilename;

    public FITGPXStreamTest(String baseFilename) {
        this.baseFilename = baseFilename;
    }

    /**
     * Test file, give extension-less file name
     */
    @Test
    public void testFile() throws IOException {
        String fit = getFITString(baseFilename + ".fit");
        String gpx = getGPXString(baseFilename  + ".gpx");
        assertTrue(new GPXProfile(fit).equals(new GPXProfile(gpx)));
    }

    /**
     * Read gpx string from .gpx file
     */
    private String getGPXString(String filename) throws IOException {
        try (
            BufferedReader is = new BufferedReader(
                new InputStreamReader(
                    getClass().getClassLoader()
                              .getResourceAsStream(filename)))
        ) {
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = is.readLine()) != null)
                sb.append(line);
            return sb.toString();
        }
    }

    /**
     * Read gpx string from .fit file using stream
     */
    abstract protected String getFITString(String filename) throws IOException;
}
