
package com.matt.fitgpxconverter.util;

import java.io.IOException;

import org.junit.Test;
import static org.junit.Assert.*;

public class CircleBufferTest {

    private void readBuf(CircleBufferInputStream buf, byte[] expected)
            throws IOException {
        byte[] read = new byte[expected.length];
        buf.read(read);
        for (int i = 0; i < expected.length; i++)
            assertEquals(read[i], expected[i]);
    }

    @Test
    public void simpleReadWrite() {
        try {
            CircleBufferInputStream buf = new CircleBufferInputStream(10);
            for (int i = 0; i < 10; i++)
                buf.write(i);
            for (int i = 0; i < 10; i++)
                assertEquals(buf.read(), i);
        } catch (IOException e) {
            System.out.println(e);
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void bulkReadWrite() {
        try {
            CircleBufferInputStream buf = new CircleBufferInputStream(10);
            byte[] ten = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            buf.write(ten);
            readBuf(buf, ten);
        } catch (IOException e) {
            System.out.println(e);
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void rotateReadWrite() {
        try {
            CircleBufferInputStream buf = new CircleBufferInputStream(10);
            buf.write(new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            readBuf(buf, new byte[] { 0, 1, 2, 3, 4 });
            buf.write(new byte[] { 10, 11, 12, 13, 14 });
            readBuf(buf, new byte[] { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
        } catch (IOException e) {
            System.out.println(e);
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void resizeSimple() {
        try {
            CircleBufferInputStream buf = new CircleBufferInputStream(10);
            byte[] ten = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            buf.write(ten);
            buf.extendBuffer(20);
            readBuf(buf, ten);
        } catch (IOException e) {
            System.out.println(e);
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void resizeRotated() {
        try {
            CircleBufferInputStream buf = new CircleBufferInputStream(10);
            buf.write(new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            readBuf(buf, new byte[] { 0, 1, 2, 3, 4 });
            buf.write(new byte[] { 10, 11, 12, 13, 14 });
            buf.extendBuffer(20);
            readBuf(buf, new byte[] { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
        } catch (IOException e) {
            System.out.println(e);
            e.printStackTrace();
            fail();
        }
    }
}
