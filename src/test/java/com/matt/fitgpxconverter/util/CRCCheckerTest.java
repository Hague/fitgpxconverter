
package com.matt.fitgpxconverter.util;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.File;

import org.junit.Test;
import static org.junit.Assert.*;

public class CRCCheckerTest {
    @Test
    public void checkCRC() {
        // TODO: load resources properly
        try (
            InputStream fis = new BufferedInputStream(
                getClass().getClassLoader()
                          .getResourceAsStream("2014-11-12-15-33-25.fit")
            );
            CRCCheckerInputStream is = new CRCCheckerInputStream(fis)
        ) {
            int b;
            do {
                b = is.read();
            } while (b > -1);
        } catch (IOException e) {
            System.out.println(e);
            e.printStackTrace();
            fail();
        }
    }
}
