
# Test FIT Files

Test FIT files have been taken from

    https://github.com/MaksVasilev/fit2gpx

and have not been modified.

Test GPX file created from FIT files using gpsbabel. Metadata tags removed from
top because creation time tag made equality tests harder.
