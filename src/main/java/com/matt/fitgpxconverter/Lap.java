
package com.matt.fitgpxconverter;

import java.time.OffsetDateTime;
import java.time.Duration;

/**
 * Data about a completed lap
 *
 * This may not have a lat/lon if the FIT file did not provide one.
 * Hence you might have to infer the coords from the most recent
 * trackpoint.
 */
public class Lap {
    private double startLat;
    private double startLon;
    private double endLat;
    private double endLon;
    private OffsetDateTime startTime = null;
    private Duration elapsedTime = null;
    private long totalDistance;

    private boolean hasEndLat = false;
    private boolean hasEndLon = false;
    private boolean hasStartLat = false;
    private boolean hasStartLon = false;
    private boolean hasTotalDistance = false;

    public double getStartLat() { return startLat; }
    public double getStartLon() { return startLon; }
    public double getEndLat() { return endLat; }
    public double getEndLon() { return endLon; }
    public OffsetDateTime getStartTime() { return startTime; }
    public Duration getElapsedTime() { return elapsedTime; }

    /**
     * Total distance in cm reportedly
     */
    public long getTotalDistance() { return totalDistance; }

    public boolean getHasStartLat() { return hasStartLat; }
    public boolean getHasStartLon() { return hasStartLon; }
    public boolean getHasEndLat() { return hasEndLat; }
    public boolean getHasEndLon() { return hasEndLon; }
    public boolean getHasStartTime() { return startTime != null; }
    public boolean getHasElapsedTime() { return elapsedTime != null; }
    public boolean getHasTotalDistance() { return hasTotalDistance; }

    public void setStartLat(double startLat) {
        this.hasStartLat = true;
        this.startLat = startLat;
    }

    public void setStartLon(double startLon) {
        this.hasStartLon = true;
        this.startLon = startLon;
    }

    public void setEndLat(double endLat) {
        this.hasEndLat = true;
        this.endLat = endLat;
    }

    public void setEndLon(double endLon) {
        this.hasEndLon = true;
        this.endLon = endLon;
    }

    public void setStartTime(OffsetDateTime startTime) {
        this.startTime = startTime;
    }

    public void setElapsedTime(Duration elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public void setTotalDistance(long totalDistance) {
        this.hasTotalDistance = true;
        this.totalDistance = totalDistance;
    }

    @Override
    public String toString() {
        return String.format(
            "Lap from %f,%f to %f,%f, started %s, elapsed %s, distance %d cm.",
            getStartLat(), getStartLon(),
            getEndLat(), getEndLon(),
            "" + getStartTime(),
            "" + getElapsedTime(),
            getTotalDistance()
        );
    }
}


