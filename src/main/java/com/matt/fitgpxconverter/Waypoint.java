
package com.matt.fitgpxconverter;

import java.time.OffsetDateTime;

public class Waypoint {
    private double lat;
    private double lon;
    private OffsetDateTime timestamp;
    private String shortname;
    private String description;

    private boolean hasAltitude = false;
    private double altitude;

    private boolean hasSpeed = false;
    private double speed;

    private boolean hasHeartrate = false;
    private short heartrate;

    private boolean hasCadence = false;
    private short cadence;

    private boolean hasPower = false;
    private int power;

    private boolean hasTemperature = false;
    private short temperature;

    private boolean isNewTrkseg = false;

    public double getLat() { return lat; }
    public double getLon() { return lon; }
    public OffsetDateTime getTimestamp() { return timestamp; }
    public String getShortname() { return shortname; }
    public String getDescription() { return description; }

    public boolean getHasAltitude() { return hasAltitude; }
    public double getAltitude() { return altitude; }

    public boolean getHasSpeed() { return hasSpeed; }
    public double getSpeed() { return speed; }

    public boolean getHasHeartrate() { return hasHeartrate; }
    public short getHeartrate() { return heartrate; }

    public boolean getHasCadence() { return hasCadence; }
    public short getCadence() { return cadence; }

    public boolean getHasPower() { return hasPower; }
    public int getPower() { return power; }

    public boolean getHasTemperature() { return hasTemperature; }
    public short getTemperature() { return temperature; }

    public boolean getIsNewTrkseg() { return isNewTrkseg; }

    public void setLat(double lat) { this.lat = lat; }
    public void setLon(double lon) { this.lon = lon; }

    public void setTimestamp(OffsetDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public void setShortname(String shortname) { this.shortname = shortname; }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAltitude(double altitude) {
        this.hasAltitude = true;
        this.altitude = altitude;
    }

    public void setSpeed(double speed) {
        this.hasSpeed = true;
        this.speed = speed;
    }

    public void setHeartrate(short heartrate) {
        this.hasHeartrate = true;
        this.heartrate = heartrate;
    }

    public void setCadence(short cadence) {
        this.hasCadence = true;
        this.cadence = cadence;
    }

    public void setPower(int power) {
        this.hasPower = true;
        this.power = power;
    }

    public void setTemperature(short temperature) {
        this.hasTemperature = true;
        this.temperature = temperature;
    }

    public void setIsNewTrkseg(boolean isNewTrkseg) {
        this.isNewTrkseg = isNewTrkseg;
    }

    @Override
    public String toString() {
        return String.format(
            "Point at %f, %f name %s.",
            getLat(), getLon(), getShortname()
        );
    }
}


