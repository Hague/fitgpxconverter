/*
    Based on gpsbabel

    Support for FIT track files.

    Copyright (C) 2020 Matthew Hague, matthewhague@zoho.com
    Copyright (C) 2011 Paul Brook, paul@nowt.org
    Copyright (C) 2003-2011    Robert Lipe, robertlipe+source@gpsbabel.org
    Copyright (C) 2019 Martin Buck, mb-tmp-tvguho.pbz@gromit.dyndns.org

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

package com.matt.fitgpxconverter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.matt.fitgpxconverter.util.CRCCheckerInputStream;
import com.matt.fitgpxconverter.util.CircleBufferInputStream;
import com.matt.fitgpxconverter.util.ExtendedDataInputStream;

import static com.matt.fitgpxconverter.util.FITConstants.*;

/**
 * Parses a FIT file as an output stream
 *
 * Usage
 *
 *  fp = FITParser(new FITListener {
 *      public void onLap(Lap lap) { ... }
 *      public void onTrackpoint(Waypoint wpt) { ... }
 *  })
 *
 *  for bytes in fit file:
 *      fp.write(bytes);
 *
 *  fp.close();
 */
public class FITParser extends OutputStream {

    // FIT file records can have at most 255 fields with 4 bytes per
    // entry. Round up to 2048 to be safe
    private static final int BUF_SIZE = 2048;
    // how frequently to flush the buffer
    private static final int FLUSH_STEPS = 1024;
    private static final int CONSUME_CHUNK_SIZE = 4096;

    // GPS time starts in 1990, not 1970
    private static final int GTIME_OFFSET = 631065600;


    public interface FITListener {
        public void onLap(Lap lap);
        public void onTrackpoint(Waypoint wpt);
    }

    public class FITConversionException extends IOException {
        public FITConversionException(String msg) {
            super(msg);
        }
    }

    private class FITField {
        public int id;
        public int size;
        public int type;

        public FITField(int id, int size, int type) {
            this.id = id;
            this.size = size;
            this.type = type;
        }
    }

    private class FITMessageDef {
        public int endian;
        public int globalID;
        public List<FITField> fields = new ArrayList<>();
    }

    private class FITData {
        public long len;
        public int endian;
        public long lastTimestamp;
        public long globalUTCOffset;
        public Map<Integer, FITMessageDef> messageDef = new HashMap<>();
    }

    private boolean newTrkseg = false;
    // buffer and is are connected -- buffer is written to, is read from
    CircleBufferInputStream buffer;
    ExtendedDataInputStream is;
    FITData fitData = new FITData();
    FITListener listener;
    private boolean headerParsed = false;
    private long markedFitDataLen = 0;

    public FITParser(FITListener listener) {
        // TODO: wrap "in" with a stream that calculates the CRC
        this.buffer = new CircleBufferInputStream(BUF_SIZE);
        this.is = new ExtendedDataInputStream(
            new CRCCheckerInputStream(buffer)
        );
        this.listener = listener;
    }

    /**
     * Try to read head from input stream
     *
     * Returns false if ran out of bytes
     */
    private boolean fitParseHeader() throws IOException {
        byte[] sig = new byte[4];

        if (is.available() < 1)
            return false;

        int len = is.readUnsignedByte();
        if (len < 12) {
            throw new FITConversionException("Bad header");
        }

        if (is.available() < len)
            return false;

        int ver = is.readUnsignedByte();
        if ((ver >> 4) > 2) {
            throw new FITConversionException(
                "Unsupported protocol version " +
                (ver >> 4) +
                (ver & 0xf)
            );
        }

        // profile version TODO: check file endianness
        ver = is.readUnsignedShort();
        // data length TODO: check file endianness
        fitData.len = is.readUnsignedIntLittle();
        // File signature
        is.readFully(sig);
        if (sig[0] != '.' || sig[1] != 'F' || sig[2] != 'I' || sig[3] != 'T') {
            throw new FITConversionException("FIT signature missing  ");
        }

        // Header CRC may be omitted entirely
        // NOTE: removed header check as it only generated warnings
        // NOTE: removed file size check as it only generated warnings

        // 12 bytes read so far, skip rest of header
        is.skipBytes(len - 12);

        fitData.globalUTCOffset = 0;

        return true;
    }

    private int fitReadUnsignedByte() throws IOException {
        --fitData.len;
        return is.readUnsignedByte();
    }

    private int fitReadUnsignedShort() throws IOException {
        fitData.len -= 2;
        return is.readUnsignedShort(fitData.endian);
    }

    private long fitReadUnsignedInt() throws IOException {
        fitData.len -= 4;
        return is.readUnsignedInt(fitData.endian);
    }

    /**
     * Returns false if ran out of bytes
     */
    private boolean fitParseDefinitionMessage(int header)
            throws IOException {

        if (is.available() < 6)
            return false;

        int localID = header & 0x0f;
        FITMessageDef def = new FITMessageDef();

        // first byte is reserved.    It's usually 0 and we don't know
        // what it is, but we've seen some files that are 0x40.    So we
        // just read it and toss it.
        fitReadUnsignedByte();

        // second byte is endianness
        def.endian = fitReadUnsignedByte();
        if (def.endian > 1) {
            throw new FITConversionException("Bad endian field 0x" + def.endian);
        }
        fitData.endian = def.endian;

        // next two bytes are the global message number
        def.globalID = fitReadUnsignedShort();

        // byte 5 has the number of records in the remainder of the
        // definition message
        int numFields = fitReadUnsignedByte();

        if (is.available() < 3 * numFields)
            return false;

        // remainder of the definition message is data at one byte per
        // field * 3 fields
        for (int i = 0; i < numFields; ++i) {
            int id = fitReadUnsignedByte();
            int size = fitReadUnsignedByte();
            int type = fitReadUnsignedByte();
            FITField field = new FITField(id, size, type);
            def.fields.add(field);
        }

        // If we have developer fields (since version 2.0) they must be
        // read too These is one byte containing the number of fields
        // and 3 bytes for every field.  So this is identical to the
        // normal fields but the meaning of the content is different.
        //
        // Currently we just want to ignore the developer fields because
        // they are not meant to hold relevant data we need (currently
        // handle) for the conversion.

        // For simplicity using the existing infrastructure we do it in
        // the following way:
        //     * We read it in as normal fields
        //     * We set the field id to kFieldInvalid so that it do not
        //     interfere with valid id's from the normal fields.
        //             -In our opinion in practice this will not happen,
        //             because we do not expect developer fields e.g.
        //             inside lap or record records. But we want to be
        //             safe here.  * We do not have to change the type
        //             as we did for the id above, because
        //             fit_read_field() already uses the size
        //             information to read the data, if the type does
        //             not match the size.
        //
        // If we want to change this or if we want to avoid the xrealloc
        // call, we can change it in the future by e.g. extending the
        // fit_message_def struct.

        // Bit 5 of the header specify if we have developer fields in
        // the data message
        boolean hasDevFields = (header & 0x20) != 0;

        if (hasDevFields) {
            if (is.available() < 1)
                return false;
            int numOfDevFields = fitReadUnsignedByte();
            if (numOfDevFields > 0) {
                if (is.available() < 3 * numOfDevFields)
                    return false;

                for (int i = 0; i < numOfDevFields; ++i) {
                    int id = fitReadUnsignedByte();
                    int size = fitReadUnsignedByte();
                    int type = fitReadUnsignedByte();
                    FITField field = new FITField(id, size, type);
                    // Because we parse developer fields like normal
                    // fields and we do not want that the field id
                    // interfere which valid id's from the normal fields
                    field.id = kFieldInvalid;
                    def.fields.add(field);
                }
            }
        }

        fitData.messageDef.put(localID, def);

        return true;
    }

    /**
     * Assumes that input stream has enough bytes.
     *
     * Callers responsibility to check
     */
    private long fitReadField(FITField f) throws IOException {
        /*
         * https://forums.garmin.com/showthread.php?223645-Vivoactive-problems-plus-suggestions-for-future-firmwares&p=610929#post610929
         * Per section 4.2.1.4.2 of the FIT Protocol the size of a field
         * may be a multiple of the size of the underlying type,
         * indicating the field contains multiple elements represented
         * as an array.
         *
         * Garmin Product Support
         */
        // In the case that the field contains one value of the
        // indicated type we return that value, otherwise we just skip
        // over the data.

        switch (f.type) {
        case 0: // enum
        case 1: // sint8
        case 2: // uint8
            if (f.size == 1) {
                return fitReadUnsignedByte();
            } else { // ignore array data
                for (int i = 0; i < f.size; ++i) {
                    fitReadUnsignedByte();
                }
                return -1;
            }
        case 0x83: // sint16
        case 0x84: // uint16
            if (f.size == 2) {
                return fitReadUnsignedShort();
            } else { // ignore array data
                for (int i = 0; i < f.size; ++i) {
                    fitReadUnsignedByte();
                }
                return -1;
            }
        case 0x85: // sint32
        case 0x86: // uint32
            if (f.size == 4) {
                return fitReadUnsignedInt();
            } else { // ignore array data
                for (int i = 0; i < f.size; ++i) {
                    fitReadUnsignedByte();
                }
                return -1;
            }
        default: // Ignore everything else for now.
            for (int i = 0; i < f.size; ++i) {
                fitReadUnsignedByte();
            }
            return -1;
        }
    }

    /**
     * Convert a time that may by system time into UTC
     *
     * If value &lt; 0x1000000 then it is system time and needs
     * converting. Uses fitData.globalUTCOffset.
     */
    private long timestampToUTC(long timestamp) {
        if (timestamp < 0x10000000)
            return timestamp + fitData.globalUTCOffset;
        else
            return timestamp;
    }

    /**
     * Returns false if ran out of bytes
     */
    private boolean fitParseData(FITMessageDef def, int timeOffset)
            throws IOException {
        long timestamp = fitData.lastTimestamp + timeOffset;
        // lat and lon deliberately signed 32 bit (not long for unsigned
        // 32 bit)
        int lat = 0x7fffffff;
        int lon = 0x7fffffff;
        int alt = 0xffff;
        int speed = 0xffff;
        short heartrate = 0xff;
        short cadence = 0xff;
        int power = 0xffff;
        short temperature = 0x7f;
        long endlat = 0x7fffffff;
        long endlon = 0x7fffffff;
        short event = 0xff;
        short eventtype = 0xff;
        long startTimestamp = 0x7fffffff;
        long elapsedTime = 0x7fffffff;
        long distance = 0x7fffffff;

        for (FITField f : def.fields) {

            if (is.available() < f.size)
                return false;
            long val = fitReadField(f);

            if (f.id == kFieldTimestamp) {
                timestamp = timestampToUTC(val);
                fitData.lastTimestamp = timestamp;
            } else {
                switch (def.globalID) {
                case kIdDeviceSettings: // device settings message
                    switch (f.id) {
                    case kFieldGlobalUtcOffset:
                        fitData.globalUTCOffset = val;
                        break;
                    default:
                        break;
                    } // switch (f.id)
                    // end of case def.global_id = kIdDeviceSettings
                    break;

                case kIdRecord: // record message - trkType is a track
                    switch (f.id) {
                    case kFieldLatitude:
                        lat = (int) val;
                        break;
                    case kFieldLongitude:
                        lon = (int) val;
                        break;
                    case kFieldAltitude:
                        if (val != 0xffff) {
                            alt = (int) val;
                        }
                        break;
                    case kFieldHeartRate:
                        heartrate = (short) val;
                        break;
                    case kFieldCadence:
                        cadence = (short) val;
                        break;
                    case kFieldDistance:
                        // NOTE: 5 is DISTANCE in cm ... unused.
                        break;
                    case kFieldSpeed:
                        if (val != 0xffff) {
                            speed = (int) val;
                        }
                        break;
                    case kFieldPower:
                        power = (int) val;
                        break;
                    case kFieldTemperature:
                        temperature = (short) val;
                        break;
                    case kFieldEnhancedSpeed:
                        if (val != 0xffff) {
                            speed = (int) val;
                        }
                        break;
                    case kFieldEnhancedAltitude:
                        if (val != 0xffff) {
                            alt = (int) val;
                        }
                        break;
                    default:
                        break;
                    } // switch (f.id)
                    // end of case def.global_id = kIdRecord
                    break;

                case kIdLap:
                    switch (f.id) {
                    case kFieldStartTime:
                        timestamp = timestampToUTC(val);
                        break;
                    case kFieldStartLatitude:
                        lat = (int) val;
                        break;
                    case kFieldStartLongitude:
                        lon = (int) val;
                        break;
                    case kFieldEndLatitude:
                        endlat = val;
                        break;
                    case kFieldEndLongitude:
                        endlon = val;
                        break;
                    case kFieldElapsedTime:
                        elapsedTime = val;
                        break;
                    case kFieldTotalDistance:
                        distance = val;
                        break;
                    default:
                        break;
                    } // switch (f.id)
                    // end of case def.global_id = kIdLap
                    break;

                case kIdEvent:
                    switch (f.id) {
                    case kFieldEvent:
                        event = (short) val;
                        break;
                    case kFieldEventType:
                        eventtype = (short) val;
                        break;
                    } // switch (f.id)
                    // end of case def.global_id = kIdEvent
                    break;
                default:
                    break;
                } // switch (def.global_id)
            }
        }

        switch (def.globalID) {
        case kIdLap: { // lap message
            Lap lap = new Lap();

            if (lat == 0x7fffffff)
                lap.setStartLat(semicircleToDegree(lat));
            if (lon == 0x7fffffff)
                lap.setStartLon(semicircleToDegree(lon));
            if (endlat != 0x7fffffff)
                lap.setEndLat(semicircleToDegree(endlat));
            if (endlon != 0x7fffffff)
                lap.setEndLon(semicircleToDegree(endlon));
            if (startTimestamp != 0x7fffffff) {
                lap.setStartTime(toOffsetDateTime(startTimestamp));
            }
            if (elapsedTime != 0x7fffffff) {
                lap.setElapsedTime(Duration.ofMillis(elapsedTime));
            }
            if (distance != 0x7fffffff) {
                lap.setTotalDistance(distance);
            }

            listener.onLap(lap);
        }
        break;
        case kIdRecord: { // record message
            if ((lat == 0x7fffffff || lon == 0x7fffffff)) {
                break;
            }

            Waypoint waypt = new Waypoint();
            if (lat != 0x7fffffff) {
                waypt.setLat(semicircleToDegree(lat));
            }
            if (lon != 0x7fffffff) {
                waypt.setLon(semicircleToDegree(lon));
            }
            if (alt != 0xffff) {
                waypt.setAltitude((alt / 5.0) - 500);
            }
            waypt.setTimestamp(toOffsetDateTime(timestamp));
            if (speed != 0xffff) {
                waypt.setSpeed(speed / 1000.0f);
            }
            if (heartrate != 0xff) {
                waypt.setHeartrate(heartrate);
            }
            if (cadence != 0xff) {
                waypt.setCadence(cadence);
            }
            if (power != 0xffff) {
                waypt.setPower(power);
            }
            if (temperature != 0x7f) {
                waypt.setTemperature(temperature);
            }
            if (newTrkseg) {
                waypt.setIsNewTrkseg(true);
                newTrkseg = false;
            }
            listener.onTrackpoint(waypt);
        }
        break;
        case kIdEvent: // event message
            if (event == kEnumEventTimer && eventtype == kEnumEventTypeStart) {
                // Start event, start new track segment. Note: We don't do this
                // on stop events because some GPS devices seem to generate a last
                // trackpoint after the stop event and that would erroneously get
                // assigned to the next segment.
                newTrkseg = true;
            }
            break;
        }

        return true;
    }

    /**
     * Returns false if input stream runs out of bytes
     */
    private boolean fitParseDataMessage(int header)
            throws IOException {
        int localID = header & 0x0f;
        if (fitData.messageDef.containsKey(localID)) {
            return fitParseData(fitData.messageDef.get(localID), 0);
        } else {
            throw new FITConversionException(
                "Message " +
                localID +
                " hasn't been defined before being used"
            );
        }
    }

    /**
     * Returns false if input stream runs out of bytes
     */
    private boolean fitParseCompressedMessage(int header)
            throws IOException {
        int localID = (header >> 5) & 3;
        if (fitData.messageDef.containsKey(localID)) {
            return fitParseData(fitData.messageDef.get(localID), header & 0x1f);
        } else {
            throw new FITConversionException(
                "Compressed message " +
                localID +
                " hasn't been defined before being used"
            );
        }
    }

    /**
     * fit_parse_record- parse each record in the file
     *
     * return false if stream "is" runs out of bytes
     */
    private boolean fitParseRecord() throws IOException {
        if (is.available() < 1)
            return false;

        int header = fitReadUnsignedByte();
        // high bit 7 set -> compressed message (0 for normal)
        // second bit 6 set -> 0 for data message, 1 for definition message
        // bit 5 -> message type specific
        //        definition message: Bit set means that we have
        //          additional Developer Field definitions behind the
        //          field definitions inside the record content
        //        data message: currently not used
        // bit 4 -> reserved
        // bits 3..0 -> local message type
        if ((header & 0x80) != 0) {
            return fitParseCompressedMessage(header);
        } else if ((header & 0x40) != 0) {
            return fitParseDefinitionMessage(header);
        } else {
            return fitParseDataMessage(header);
        }
    }

    @Override
    public void write(byte[] bytes, int off, int len) throws IOException {
        for (int i = off; i < off + len; i += FLUSH_STEPS) {
            int chunkLen = Math.min(FLUSH_STEPS, off + len - i);
            buffer.write(bytes, i, chunkLen);
            if (buffer.available() >= FLUSH_STEPS)
                flushBuffer();
        }
    }

    /**
     * Write FIT file here byte by byte
     *
     * Or use bulk write methods...
     */
    @Override
    public void write(int b) throws IOException {
        buffer.write(b & 0xff);
        if (buffer.available() >= FLUSH_STEPS) {
            flushBuffer();
        }
    }

    @Override
    public void close() throws IOException {
        flushBuffer();
        if (fitData.len > 0)
            throw new FITConversionException("OutputStream closed before end of data");
        // close input stream to force CRC check
        buffer.markEOF();
        is.close();
    }


    @Override
    public void flush() throws IOException {
        flushBuffer();
    }

    /**
     * Consume input stream and write it to FITParser
     */
    public void consumeInputStream(InputStream in) throws IOException {
        byte[] bytes = new byte[CONSUME_CHUNK_SIZE];
        int read;
        do {
            read = in.read(bytes);
            if (read > -1)
                write(bytes, 0, read);
        } while (read > -1);
        flush();
    }

    private void flushBuffer() throws IOException {
        // try to read the header or record by record, if we run out of
        // bytes, reset to last complete point
        if (!headerParsed) {
            mark();
            headerParsed = fitParseHeader();
            if (!headerParsed) {
                reset();
                return;
            }
        }

        boolean passed = true;
        while (passed && fitData.len > 0) {
            mark();
            passed = fitParseRecord();
        }
        if (!passed)
            reset();
        else // reached end of fit file, so skip rest
            is.skip(is.available());
    }

    // borrowed from
    // https://github.com/MaksVasilev/fit2gpx
    private double semicircleToDegree(long value) {
        // degrees = semicircles * ( 180 / 2^31 )
        return value * (180.0 / Math.pow(2.0, 31.0));
    }

    private void mark() {
        is.mark(BUF_SIZE);
        markedFitDataLen = fitData.len;
    }

    private void reset() throws IOException {
        is.reset();
        fitData.len = markedFitDataLen;
    }

    private OffsetDateTime toOffsetDateTime(long timestamp) {
        return OffsetDateTime.of(
            LocalDateTime.ofEpochSecond(
                timestamp  + GTIME_OFFSET,
                0,
                ZoneOffset.UTC),
            ZoneOffset.UTC
        );
    }
}
