
package com.matt.fitgpxconverter;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import com.matt.fitgpxconverter.util.DefaultFITListener;
import com.matt.fitgpxconverter.util.GPXWriter;

/**
 * Convert FIT output into GPX output
 */
public class FITGPXOutputStream extends OutputStream {

    private static final Charset DEFAULT_ENCODING = StandardCharsets.UTF_8;

    private GPXWriter gpxWriter;
    private FITParser parser;

    /**
     * Create an output stream
     *
     * Write binary FIT file to stream, outputs GPX in UTF-8
     */
    public FITGPXOutputStream(OutputStream out)
            throws IOException {
        this(out, DEFAULT_ENCODING);
    }

    /**
     * Create an output stream
     *
     * Write binary FIT file to stream, outputs GPX in specified
     * encoding to out
     */
    public FITGPXOutputStream(OutputStream out, Charset charset)
            throws IOException {
        gpxWriter = new GPXWriter(out, charset);
        parser = new FITParser(new DefaultFITListener(gpxWriter));
    }

    @Override
    public void write(int b) throws IOException {
        parser.write(b & 0xff);
    }

    @Override
    public void write(byte[] bytes, int off, int len) throws IOException {
        parser.write(bytes, off, len);
    }

    @Override
    public void close() throws IOException {
        parser.close();
        gpxWriter.close();
    }

    @Override
    public void flush() throws IOException {
        parser.flush();
        gpxWriter.flush();
    }
}
