
package com.matt.fitgpxconverter;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.StringBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import com.matt.fitgpxconverter.util.DefaultFITListener;
import com.matt.fitgpxconverter.util.GPXWriter;

public class FITGPXInputStream extends InputStream {

    private static final Charset DEFAULT_ENCODING = StandardCharsets.UTF_8;

    private static final int CHUNK_SIZE = 1024;
    // don't fill up string buf more if it has this many chars already
    private static final int STRING_BUF_LIMIT = 1024;

    private InputStream in;
    private StringBuffer buffer;
    private GPXWriter gpxWriter;
    private FITParser parser;
    private Charset charset;
    private boolean eofReached = false;
    private byte[] chunk = new byte[CHUNK_SIZE];
    private byte[] oneByteBuf = new byte[1];

    // since 1 character might encode to several bytes
    // use this to store "left over bytes" to be put into the next read
    private byte[] encodingBuffer;
    private int encodingBufferOffset;

    /**
     * Construct a FIT to GPX input stream
     *
     * The input stream is always binary encoded, the output will be
     * UTF-8.
     */
    public FITGPXInputStream(InputStream in) throws IOException {
        this(in, DEFAULT_ENCODING);
    }

    /**
     * Construct a FIT to GPX input stream
     *
     * Specify what encoding you want the GPX file in. The input file is
     * always binary encoded.
     */
    public FITGPXInputStream(InputStream in, Charset charset)
            throws IOException {
        this.in = in;
        this.charset = charset;

        // Write GPX files to writer, read them from buffer
        StringWriter wr = new StringWriter();
        gpxWriter = new GPXWriter(wr, charset);
        buffer = wr.getBuffer();

        parser = new FITParser(new DefaultFITListener(gpxWriter));
    }

    @Override
    public int read() throws IOException {
        int numRead = readBytes(oneByteBuf, 0, 1);

        if (numRead < 0)
            return -1;
        else
            return oneByteBuf[0] & 0xff;
    }

    @Override
    public int read(byte[] bytes, int off, int len) throws IOException {
        return readBytes(bytes, off, len);
    }

    @Override
    public void close() throws IOException {
        parser.close();
        gpxWriter.close();
        in.close();
    }

    /**
     * Try to read num bytes into bytes from off(set)
     */
    private int readBytes(byte[] bytes, int off, int num)
            throws IOException {

        // this turns out complicated...
        // first try to read as much as possible from the left over
        // bytes buffer (the purpose of which is revealed shortly).
        // Then try to make sure buffer contains as many characters as
        // num bytes needed.
        // Next, encode those characters as UTF-8 bytes. This may cause
        // more bytes than are needed as some characters need multiple
        // bytes.
        // Copy as many needed bytes into the bytes array. Save the rest
        // in the leftover bytes buffer.

        if (num == 0)
            return 0;

        int read = readFromEncodingBuffer(bytes, off, num);
        return read + readFromStringBuffer(bytes, off + read, num - read);
    }

    /**
     * Try to read num bytes from encoding buffer
     *
     * Returns num read
     */
    private int readFromEncodingBuffer(byte[] bytes, int off, int num) {
        int read = 0;
        if (encodingBuffer != null) {
            read = Math.min(bytes.length,
                            encodingBuffer.length - encodingBufferOffset);
            System.arraycopy(encodingBuffer,
                             encodingBufferOffset,
                             bytes,
                             off,
                             read);
            encodingBufferOffset += read;
            if (encodingBufferOffset >= encodingBuffer.length)
                encodingBuffer = null;
        }
        return read;
    }

    /**
     * Read num bytes from string buffer
     *
     * Assumes encoding buffer has been emptied. Writes a new encoding
     * buffer if bytes were left over from this operation.
     *
     * Returns num bytes read or -1 if end
     */
    private int readFromStringBuffer(byte[] bytes, int off, int num)
            throws IOException {
        int read = 0;
        boolean hasMore = true;

        while (num > 0 && hasMore) {
            // get some more bytes into buffer if needed
            if (buffer.length() < num && buffer.length() < STRING_BUF_LIMIT) {
                hasMore = readChunk();
                // flush out end of gpx by declaring no more input to
                // parser and closing the writer
                if (!hasMore && !eofReached) {
                    parser.close();
                    gpxWriter.close();
                    eofReached = true;
                    hasMore = true;
                }
            }

            int len = Math.min(num, buffer.length());

            // this feels inefficient
            byte[] subBuf = buffer.substring(0, len)
                                  .getBytes(charset);
            buffer.delete(0, len);

            int numBytes = Math.min(num, subBuf.length);
            System.arraycopy(subBuf, 0, bytes, off + read, numBytes);

            read += numBytes;
            num -= numBytes;

            if (numBytes < subBuf.length) {
                encodingBuffer = subBuf;
                encodingBufferOffset = numBytes;
            }
        }

        // if we didn't manage to read anything we reached the end
        if (read == 0)
            return -1;
        else
            return read;
    }

    /**
     * Read and parse a chunk from the input stream
     *
     * Fills up buffer with the results. Returns false if EOF.
     */
    private boolean readChunk() throws IOException {
        int numRead = in.read(chunk);

        if (numRead < 0) {
            return false;
        } else if (numRead > 0) {
            parser.write(chunk, 0, numRead);
            parser.flush();
        }
        return true;
    }
}
