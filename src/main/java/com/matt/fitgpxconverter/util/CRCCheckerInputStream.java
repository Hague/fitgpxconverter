/*
    Based on gpsbabel

    Support for FIT track files.

    Copyright (C) 2020 Matthew Hague, matthewhague@zoho.com
    Copyright (C) 2011 Paul Brook, paul@nowt.org
    Copyright (C) 2003-2011    Robert Lipe, robertlipe+source@gpsbabel.org
    Copyright (C) 2019 Martin Buck, mb-tmp-tvguho.pbz@gromit.dyndns.org

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

package com.matt.fitgpxconverter.util;

import java.io.IOException;
import java.io.InputStream;

/**
 * Checks the CRC of a FIT file passing through
 *
 * Throws an CRCException if an error is spotted
 */
public class CRCCheckerInputStream extends InputStream {
    public class CRCException extends IOException {
        public CRCException(String msg) {
            super(msg);
        }
    }

    private static final int[] crcTable = {
        0x0000, 0xcc01, 0xd801, 0x1400, 0xf001, 0x3c00, 0x2800, 0xe401,
        0xa001, 0x6c00, 0x7800, 0xb401, 0x5000, 0x9c01, 0x8801, 0x4400
    };

    private InputStream is;
    private int crc = 0;
    private int markedCrc = 0;
    private int count;
    private int markedCount;

    public CRCCheckerInputStream(InputStream is) {
        this.is = is;
    }

    @Override
    public int read() throws IOException {
        int b = is.read();
        addCRCByte(b);
        return b;
    }

    @Override
    public int read(byte[] buffer, int off, int len) throws IOException {
        int num = is.read(buffer, off, len);
        for (int i = 0; i < num; i++)
            addCRCByte((int)(buffer[off + i]) & 0xff);
        return num;
    }

    @Override
    public boolean markSupported() {
        return is.markSupported();
    }

    @Override
    public void mark(int readLimit) {
        markedCrc = crc;
        markedCount = count;
        is.mark(readLimit);
    }

    @Override
    public void reset() throws IOException {
        crc = markedCrc;
        count = markedCount;
        is.reset();
    }

    @Override
    public int available() throws IOException {
        return is.available();
    }

    @Override
    public void close() throws IOException {
        // flush last few bytes from buffer
        int b;
        do {
            b = read();
        } while (b > -1);

        checkCRC();
        is.close();
    }

    private void addCRCByte(int b) {
        if (b < 0)
            return;

        crc = (crc >> 4) ^ crcTable[crc & 0xf] ^ crcTable[b & 0xf];
        crc = (crc >> 4) ^ crcTable[crc & 0xf] ^ crcTable[(b >> 4) & 0xf];
        count += 1;
    }

    private void checkCRC() throws IOException {
        if (crc != 0)
            throw new CRCException("CRC Mismatch " + crc + " on " + count + " bytes available " + is.available());
    }
}

