
package com.matt.fitgpxconverter.util;

import java.io.BufferedWriter;
import java.io.Flushable;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

import com.matt.fitgpxconverter.Waypoint;

/**
 * Writes GPX files to the given writer
 *
 * Call close to finish the file
 */
public class GPXWriter implements AutoCloseable, Flushable {

    private static final Charset DEFAULT_ENCODING = StandardCharsets.UTF_8;

    // Needs printf-ing with charset
    private final String OUT_GPX_HEAD
        = "<?xml version=\"1.0\" encoding=\"%s\"?>\n" +
          "<gpx creator=\"Converted by FITGPXConverter\" version=\"1.1\" " +
          "xmlns=\"http://www.topografix.com/GPX/1/1\" " +
          "xmlns:gpxtrx=\"http://www.garmin.com/xmlschemas/GpxExtensions/v3\" " +
          "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
          "xmlns:gpxtpx=\"http://www.garmin.com/xmlschemas/TrackPointExtension/v1\" " +
          "xmlns:gpxx=\"http://www.garmin.com/xmlschemas/WaypointExtension/v1\" " +
          "xmlns:nmea=\"http://trekbuddy.net/2009/01/gpx/nmea\">\n" +
          " <trk>" +
          "  <trkseg>";

    private static final String OUT_GPX_NEW_SEG = "  </trkseg>\n  <trkseg>\n";

    private static final String OUT_GPX_TRK_TAIL
        = "  </trkseg>\n </trk>\n";
    private static final String OUT_GPX_TAIL = "</gpx>\n";

    private PrintWriter out;

    // I worked so hard to avoid buffering the whole file, but we need
    // to move all waypoints (not trackpoints outside of the track).
    // There hopefully won't be too many of these.
    private List<Waypoint> waypoints = new LinkedList<>();

    public GPXWriter(OutputStream out, Charset charset) {
        this.out = new PrintWriter(
            new BufferedWriter(
                new OutputStreamWriter(out, charset)));
        this.out.printf(OUT_GPX_HEAD, charset);
    }

    public GPXWriter(OutputStream out) {
        this(out, DEFAULT_ENCODING);
    }

    /**
     * Charset doesn't matter so much for a writer, just affects what
     * appears in the GPX encoding line
     */
    public GPXWriter(Writer out, Charset charset) {
        this.out = new PrintWriter(out);
        this.out.printf(OUT_GPX_HEAD, charset);
    }

    public GPXWriter(Writer out) {
        this(out, DEFAULT_ENCODING);
    }

    public void writeWaypoint(Waypoint pt) {
        waypoints.add(pt);
    }

    public void writeTrackpoint(Waypoint pt) {
        if (pt.getIsNewTrkseg())
            out.printf(OUT_GPX_NEW_SEG);
        outputPoint(pt, "   ", "trkpt");
    }

    /**
     * General method for outputting points
     *
     * @param indent prefix all lines with this string
     * @param tag The root xml tag (trkpt, wpt, rtept)
     */
    private void outputPoint(Waypoint pt, String indent, String tag) {
        out.printf("%s<%s lat=\"%f\" lon=\"%f\">\n",
                   indent, tag, pt.getLat(), pt.getLon());

        String name = pt.getShortname();

        String description = pt.getDescription();
        if (description == null)
            description = name;

        if (name != null) {
            out.printf("%s <name>%s</name>\n", indent, name);
            out.printf("%s <cmt>%s</cmt>\n", indent, name);
            out.printf("%s <desc>%s</desc>\n", indent, description);
        }

        OffsetDateTime ts = pt.getTimestamp();
        if (ts != null) {
            // OffsetDateTime to string is in required UTC and
            out.printf(
                "%s <time>%s</time>\n",
                indent,
                DateTimeFormatter.ISO_INSTANT.format(
                    ZoneOffset.UTC.adjustInto(ts)
                )
            );
        }

        if (pt.getHasAltitude())
            out.printf("%s <ele>%.3f</ele>\n", indent, pt.getAltitude());

        if (pt.getHasSpeed())
            out.printf("%s <speed>%.6f</speed>\n", indent, pt.getSpeed());

        writeExtensions(pt);
        out.printf("%s</%s>\n", indent, tag);
    }

    private void writeExtensions(Waypoint pt) {
        if (!(pt.getHasPower() ||
              pt.getHasSpeed() ||
              pt.getHasTemperature() ||
              pt.getHasHeartrate() ||
              pt.getHasCadence()))
            return;

        out.printf("    <extensions>\n");

        if (pt.getHasPower())
            out.printf("     <power>%d</power>\n", pt.getPower());

        if (pt.getHasTemperature() ||
            pt.getHasHeartrate() ||
            pt.getHasCadence()) {

            out.printf("     <gpxtpx:TrackPointExtension>\n");

            if (pt.getHasTemperature())
                out.printf("      <gpxtpx:atemp>%d</gpxtpx:atemp>\n",
                           pt.getTemperature());

            if (pt.getHasHeartrate())
                out.printf("      <gpxtpx:hr>%d</gpxtpx:hr>\n",
                           pt.getHeartrate());

            if (pt.getHasCadence())
                out.printf("      <gpxtpx:cad>%d</gpxtpx:cad>\n",
                           pt.getCadence());

            out.printf("     </gpxtpx:TrackPointExtension>\n");
        }

        out.printf("    </extensions>\n");
    }

    @Override
    public void close() throws IOException {
        out.printf(OUT_GPX_TRK_TAIL);
        for (Waypoint pt : waypoints)
            outputPoint(pt, " ", "wpt");
        out.printf(OUT_GPX_TAIL);
        out.close();
    }

    @Override
    public void flush() throws IOException {
        out.flush();
    }
}
