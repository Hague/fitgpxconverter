package com.matt.fitgpxconverter.util;

import java.time.format.DateTimeFormatter;

import com.matt.fitgpxconverter.FITParser;
import com.matt.fitgpxconverter.Lap;
import com.matt.fitgpxconverter.Waypoint;

/**
 * Provide a basic listener that sends FIT/GPX to GPXWriter
 */
public class DefaultFITListener implements FITParser.FITListener {

    private GPXWriter gpxWriter;

    // remember last lat/lon for lap end points that don't have
    // lat/lon
    private Waypoint lastTrackpoint = null;
    private int lapCt = 0;

    public DefaultFITListener(GPXWriter gpxWriter) {
        this.gpxWriter = gpxWriter;
    }

    public void onLap(Lap lap) {
        Waypoint pt = new Waypoint();

        String name = String.format("LAP%03d", ++lapCt);

        pt.setShortname(name);

        if (lap.getHasEndLat())
            pt.setLat(lap.getEndLat());
        else if (lastTrackpoint != null)
            pt.setLat(lastTrackpoint.getLat());
        else
            return;

        if (lap.getHasEndLon())
            pt.setLon(lap.getEndLon());
        else if (lastTrackpoint != null)
            pt.setLon(lastTrackpoint.getLon());
        else
            return;

        if (lap.getHasStartTime() && lap.getHasElapsedTime()) {
            pt.setTimestamp(lap.getStartTime().plus(lap.getElapsedTime()));
        }

        if (lap.getHasElapsedTime()) {
            pt.setDescription(name + " Duration " + lap.getElapsedTime());
        }

        gpxWriter.writeWaypoint(pt);
    }

    public void onTrackpoint(Waypoint pt) {
        lastTrackpoint = pt;
        gpxWriter.writeTrackpoint(pt);
    }
}

