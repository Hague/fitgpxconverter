/*
    Adapted from gpsbabel.

    Support for FIT track files.

    Copyright (C) 2020 Matthew Hague, matthewhague@zoho.com
    Copyright (C) 2011 Paul Brook, paul@nowt.org
    Copyright (C) 2003-2011    Robert Lipe, robertlipe+source@gpsbabel.org
    Copyright (C) 2019 Martin Buck, mb-tmp-tvguho.pbz@gromit.dyndns.org

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA    02110-1301, USA.

*/

package com.matt.fitgpxconverter.util;

import java.util.Map;
import java.util.HashMap;

public class FITConstants {

// constants for global IDs
    public static final int kIdFileId = 0;
    public static final int kIdDeviceSettings = 0;
    public static final int kIdLap = 19;
    public static final int kIdRecord = 20;
    public static final int kIdEvent = 21;
    public static final int kIdCourse = 31;
    public static final int kIdCoursePoint = 32;

// constants for local IDs (for writing)
    public static final int kWriteLocalIdFileId = 0;
    public static final int kWriteLocalIdCourse = 1;
    public static final int kWriteLocalIdLap = 2;
    public static final int kWriteLocalIdEvent = 3;
    public static final int kWriteLocalIdCoursePoint = 4;
    public static final int kWriteLocalIdRecord = 5;

// constants for message fields
// for all global IDs
    public static final int kFieldTimestamp = 253;
    public static final int kFieldMessageIndex = 254;
// for global ID: file id
    public static final int kFieldType = 0;
    public static final int kFieldManufacturer = 1;
    public static final int kFieldProduct = 2;
    public static final int kFieldTimeCreated = 4;
// for global ID: device settings
    public static final int kFieldGlobalUtcOffset = 4;
// for global ID: lap
    public static final int kFieldStartTime = 2;
    public static final int kFieldStartLatitude = 3;
    public static final int kFieldStartLongitude = 4;
    public static final int kFieldEndLatitude = 5;
    public static final int kFieldEndLongitude = 6;
    public static final int kFieldElapsedTime = 7;
    public static final int kFieldTotalTimerTime = 8;
    public static final int kFieldTotalDistance = 9;
    public static final int kFieldAvgSpeed = 13;
    public static final int kFieldMaxSpeed = 14;
// for global ID: record
    public static final int kFieldLatitude = 0;
    public static final int kFieldLongitude = 1;
    public static final int kFieldAltitude = 2;
    public static final int kFieldHeartRate = 3;
    public static final int kFieldCadence = 4;
    public static final int kFieldDistance = 5;
    public static final int kFieldSpeed = 6;
    public static final int kFieldPower = 7;
    public static final int kFieldTemperature = 13;
    public static final int kFieldEnhancedSpeed = 73;
    public static final int kFieldEnhancedAltitude = 78;
// for global ID: event
    public static final int kFieldEvent = 0;
    public static final int kEnumEventTimer = 0;
    public static final int kFieldEventType = 1;
    public static final int kEnumEventTypeStart = 0;
    public static final int kFieldEventGroup = 4;
// for global ID: course
    public static final int kFieldSport = 4;
    public static final int kFieldName = 5;
// for global ID: course point
    public static final int kFieldCPTimeStamp = 1;
    public static final int kFieldCPPositionLat = 2;
    public static final int kFieldCPPositionLong = 3;
    public static final int kFieldCPDistance = 4;
    public static final int kFieldCPName = 6;
    public static final int kFieldCPType = 5;

// For developer fields as a non conflicting id
    public static final int kFieldInvalid = 255;

// types for message definitions
    public static final int kTypeEnum = 0x00;
    public static final int kTypeUint8 = 0x02;
    public static final int kTypeString = 0x07;
    public static final int kTypeUint16 = 0x84;
    public static final int kTypeSint32 = 0x85;
    public static final int kTypeUint32 = 0x86;

// misc. constants for message fields
    public static final int kFileCourse = 0x06;
    public static final int kEventTimer = 0x00;
    public static final int kEventTypeStart = 0x00;
    public static final int kEventTypeStopDisableAll = 0x09;
    public static final int kCoursePointTypeGeneric = 0x00;
    public static final int kCoursePointTypeLeft = 0x06;
    public static final int kCoursePointTypeRight = 0x07;

    public static final int kWriteHeaderLen = 12;
    public static final int kWriteHeaderCrcLen = 14;
    public static final int kReadHeaderCrcLen = 14;

    public static final double kSynthSpeed = 10.0 * 1000 / 3600; /* speed in m/s */

    public static Map<String, Integer> kCoursePointTypeMapping
        = new HashMap<>();
    static {
        kCoursePointTypeMapping.put("left", kCoursePointTypeLeft);
        kCoursePointTypeMapping.put("links", kCoursePointTypeLeft);
        kCoursePointTypeMapping.put("gauche", kCoursePointTypeLeft);
        kCoursePointTypeMapping.put("izquierda", kCoursePointTypeLeft);
        kCoursePointTypeMapping.put("sinistra", kCoursePointTypeLeft);

        kCoursePointTypeMapping.put("right", kCoursePointTypeRight);
        kCoursePointTypeMapping.put("rechts", kCoursePointTypeRight);
        kCoursePointTypeMapping.put("droit", kCoursePointTypeRight);
        kCoursePointTypeMapping.put("derecha", kCoursePointTypeRight);
        kCoursePointTypeMapping.put("destro", kCoursePointTypeRight);
    }
}

