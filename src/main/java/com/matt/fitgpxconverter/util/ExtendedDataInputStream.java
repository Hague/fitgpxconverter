
package com.matt.fitgpxconverter.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.DataInputStream;

/**
 * DataInputStream for little or big endian
 *
 * Adapted from
 * https://www.peterfranza.com/2008/09/26/little-endian-input-stream/
 *
 * All methods ending "Little" read in little endian, plus some methods
 * that take 0 (little endian) or 1 (big endian)
 */
public class ExtendedDataInputStream extends DataInputStream {
    private byte w[]; // work array for buffering input

    public ExtendedDataInputStream(InputStream is) {
        super(is);
        w = new byte[8];
    }

    /**
     * Reads 4 bytes and returns as long (unsigned)
     */
    public long readUnsignedInt() throws IOException {
        long val = readInt();
        if (val < 0)
            val += 2 * (Integer.MAX_VALUE + 1L);
        return val;
    }

    /**
     * Reads 4 bytes and returns as long (unsigned)
     */
    public long readUnsignedIntLittle() throws IOException {
        long val = readIntLittle();
        if (val < 0)
            val += 2 * (Integer.MAX_VALUE + 1L);
        return val;
    }

    public final short readShortLittle() throws IOException {
        readFully(w, 0, 2);
        return (short)(
                (w[1]&0xff) << 8 |
                (w[0]&0xff));
    }

    /**
     * Note, returns int even though it reads a short.
     */
    public final int readUnsignedShortLittle() throws IOException {
        readFully(w, 0, 2);
        return (
                (w[1]&0xff) << 8 |
                (w[0]&0xff));
    }

    /**
     * like DataInputStream.readChar except little endian.
     */
    public final char readCharLittle() throws IOException {
        readFully(w, 0, 2);
        return (char) (
                (w[1]&0xff) << 8 |
                (w[0]&0xff));
    }

    /**
     * like DataInputStream.readInt except little endian.
     */
    public final int readIntLittle() throws IOException {
        readFully(w, 0, 4);
        return
        (w[3])      << 24 |
        (w[2]&0xff) << 16 |
        (w[1]&0xff) <<  8 |
        (w[0]&0xff);
    }

    /**
     * like DataInputStream.readLong except little endian.
     */
    public final long readLongLittle() throws IOException {
        readFully(w, 0, 8);
        return
        (long)(w[7])      << 56 |
        (long)(w[6]&0xff) << 48 |
        (long)(w[5]&0xff) << 40 |
        (long)(w[4]&0xff) << 32 |
        (long)(w[3]&0xff) << 24 |
        (long)(w[2]&0xff) << 16 |
        (long)(w[1]&0xff) <<  8 |
        (long)(w[0]&0xff);
    }

    public final float readFloatLittle() throws IOException {
        return Float.intBitsToFloat(readIntLittle());
    }

    public final double readDoubleLittle() throws IOException {
        return Double.longBitsToDouble(readLongLittle());
    }

    public long readUnsignedInt(int endian) throws IOException {
        if (endian == 0)
            return readUnsignedIntLittle();
        else
            return readUnsignedInt();
    }

    public final short readShort(int endian) throws IOException {
        if (endian == 0)
            return readShortLittle();
        else
            return readShort();
    }

     public final int readUnsignedShort(int endian) throws IOException {
         if (endian == 0)
             return readUnsignedShortLittle();
         else
             return readUnsignedShort();
    }

     public final char readChar(int endian) throws IOException {
         if (endian == 0)
             return readCharLittle();
         else
             return readChar();
    }

    public final int readInt(int endian) throws IOException {
        if (endian == 0)
            return readIntLittle();
        else
            return readInt();
    }

    public final long readLong(int endian) throws IOException {
        if (endian == 0)
            return readLongLittle();
        else
            return readLong();
    }

    public final float readFloat(int endian) throws IOException {
        if (endian == 0)
            return readFloatLittle();
        else
            return readFloat();
    }

    public final double readDouble(int endian) throws IOException {
        if (endian == 0)
            return readDoubleLittle();
        else
            return readDouble();
    }
}
