
package com.matt.fitgpxconverter.util;

import java.io.InputStream;
import java.io.IOException;

/**
 * A buffer-based input stream
 *
 * Bytes can be placed into the buffer for reading with the usual read
 * methods. Will throw an exception if the read head catches up with the
 * write head, so make your buffer big enough for this not to happen.
 *
 * Similarly this will happen if the write head catches up with the read
 * head.
 *
 * Mark the end of input by calling markEOF(). close() is not used here
 * as close belongs to the input stream side of things.
 */
public class CircleBufferInputStream extends InputStream {

    private int[] buffer;
    private int readHead;
    private int writeHead;
    private int bufSize;
    private int mark = -1;
    private boolean eofReached = false;

    public CircleBufferInputStream(int bufSize) {
        // +1 as write head = read head means empty
        this.bufSize = bufSize + 1;
        this.buffer = new int[bufSize + 1];
        this.readHead = 0;
        this.writeHead = 0;
        this.mark = 0;
    }

    @Override
    public int read() throws IOException {
        if (readHead == writeHead) {
            if (eofReached)
                return -1;
            else
                throw new IOException("Circular buffer read head caught up with write head");
        }

        int result = buffer[readHead];
        readHead = (readHead + 1) % bufSize;
        return result;
    }

    @Override
    public int available() {
        int available;
        if (writeHead >= readHead)
            available = writeHead - readHead;
        else
            available = bufSize - (readHead - writeHead) - 1;
        return available;
    }

    @Override
    public void mark(int readLimit) {
        // if readLimit bigger than the buffer, increase the buffer!
        extendBuffer(readLimit);
        this.mark = this.readHead;
    }

    @Override
    public boolean markSupported() { return true; }

    @Override
    public void reset() throws IOException {
        if (mark < 0)
            throw new IOException("Mark not set or invalidated");
        this.readHead = mark;
    }

    /**
     * Register that no more bytes are coming
     */
    public void markEOF() {
        eofReached = true;
    }

    /**
     * Write a byte to the input stream
     *
     * Throws an IO exception if the write head catches up with the read
     * head
     */
    public void write(int b) throws IOException {
        if (eofReached)
            throw new IOException("Can't write when EOF has been marked");

        if (writeHead == mark)
            mark = -1;

        buffer[writeHead] = (b & 0xff);
        writeHead = (writeHead + 1) % bufSize;

        if (writeHead == readHead)
            throw new IOException("Circular buffer write head caught up with read head.");
    }

    /**
     * Write a load of bytes
     */
    public void write(byte[] bytes, int off, int len) throws IOException {
        if (eofReached)
            throw new IOException("Can't write when EOF has been marked");

        if (len > bufSize - available())
            throw new IOException("CircleBufferInputStream overflow");

        int toEnd = Math.min(bufSize - writeHead, len);
        for (int i = 0; i < len; i++)
            buffer[(writeHead + i) % bufSize] = bytes[off + i] & 0xff;

        writeHead = (writeHead + len) % bufSize;
    }

    /**
     * Write a load of bytes
     */
    public void write(byte[] bytes) throws IOException {
        write(bytes, 0, bytes.length);
    }

    /**
     * Increase buffer to newSize if it's not bigger already
     */
    public void extendBuffer(int newSize) {
        if (newSize <= this.bufSize)
            return;

        int[] newBuf = new int[newSize];

        // can just copy across if writeHead is ahead, if wrapped, more
        // complicated as we don't want readHead running into unwritten
        // extension
        if (writeHead > readHead) {
            System.arraycopy(buffer, 0, newBuf, 0, bufSize);
        } else {
            // rearrange
            // aaaaaa wh bbbbbb rh cccccc
            // to
            // rh cccccc aaaaaa wh bbbbbb ..new buf...
            int toEnd = bufSize - readHead;
            System.arraycopy(buffer, readHead, newBuf, 0, toEnd);
            System.arraycopy(buffer, 0, newBuf, toEnd, readHead);
            readHead = 0;
            writeHead += toEnd;
        }

        buffer = newBuf;
        bufSize = newSize;
    }
}
