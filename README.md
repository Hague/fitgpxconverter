
# FIT to GPX Converter

A Java library for converting ANT FIT files into GPX format which does not rely
on the ANT SDK.

Conversion code ported from [gpsbabel][gpsbabel].

## Import to Your Project

[JitPack.io][jitpack] can be used to import the library to your
gradle/maven/sbt/leinengen project. See [this page][fitgpxconverter-jitpack].

## Usage

There are three main modes of usage: parser, input stream, output stream.

### Parser

A parser is provided with callbacks

    import com.matt.fitgpxconverter.FITParser;
    import com.matt.fitgpxconverter.Waypoint;

    ...

    FITParser parser = new FITParser(new FITParser.FITListener() {
        public void onWaypoint(Waypoint pt) {
            // use waypoint
        }
        public void onTrackpoint(Waypoint pt) {
            // use trackpoint
        }
    });

The `FITParser` object can either consume an input stream, or be written to
with the `OutputStream` interface.

    parser.consumeInputStream(myFITInputStream);
    parser.close();

or

    for (byte[] bytes : myFITBytes)
        parser.write(bytes);
    parser.close();

### Input Stream

Converts an input stream containing FIT data into GPX data.

    import com.matt.fitgpxconverter.FITGPXInputStream;

    ...

    InputStream in = new FITGPXInputStream(
        new FileInputStream("myfile.fit")
    );

    // Read GPX data

    in.close();

The source input stream is consumed in 4K chunks, so more buffering is likely
not needed.

### Output Stream

Converts an output stream for outputting FIT data to output GPX data.

    import com.matt.fitgpxconverter.FITGPXOutputStream;

    ...

    OutputStream out = new FITGPXOutputStream(
        new FileOutputStream("myfile.gpx")
    );

    // write FIT data

    in.close();

Output is via a buffered writer, so extra buffering should not usually be needed.

[gpsbabel]: https://www.gpsbabel.org/
[jitpack]: https://jitpack.io/
[fitgpxconverter-jitpack]: https://jitpack.io/#com.gitlab.Hague/fitgpxconverter/
